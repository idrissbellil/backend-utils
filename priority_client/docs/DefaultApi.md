# priority_client.DefaultApi

All URIs are relative to *https://virtserver.swaggerhub.com/idriss-endpoint/rev-ai/1.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**priority_code_get**](DefaultApi.md#priority_code_get) | **GET** /{priorityCode} | 
[**root_get**](DefaultApi.md#root_get) | **GET** / | 


# **priority_code_get**
> PriorityCodeExist priority_code_get(priority_code)



Returns 1 if priority code exists, otherwise returns 0

### Example
```python
from __future__ import print_function
import time
import priority_client
from priority_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = priority_client.DefaultApi()
priority_code = 'priority_code_example' # str | 

try:
    api_response = api_instance.priority_code_get(priority_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->priority_code_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **priority_code** | **str**|  | 

### Return type

[**PriorityCodeExist**](PriorityCodeExist.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **root_get**
> list[PriorityCode] root_get()



Returns a list of all possible priority codes

### Example
```python
from __future__ import print_function
import time
import priority_client
from priority_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = priority_client.DefaultApi()

try:
    api_response = api_instance.root_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->root_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[PriorityCode]**](PriorityCode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

