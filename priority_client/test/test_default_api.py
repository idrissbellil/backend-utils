# coding: utf-8

"""
    

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import priority_client
from priority_client.api.default_api import DefaultApi  # noqa: E501
from priority_client.rest import ApiException


class TestDefaultApi(unittest.TestCase):
    """DefaultApi unit test stubs"""

    def setUp(self):
        self.api = priority_client.api.default_api.DefaultApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_priority_code_get(self):
        """Test case for priority_code_get

        """
        pass

    def test_root_get(self):
        """Test case for root_get

        """
        pass


if __name__ == '__main__':
    unittest.main()
