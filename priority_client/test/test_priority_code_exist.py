# coding: utf-8

"""
    

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import priority_client
from priority_client.models.priority_code_exist import PriorityCodeExist  # noqa: E501
from priority_client.rest import ApiException


class TestPriorityCodeExist(unittest.TestCase):
    """PriorityCodeExist unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPriorityCodeExist(self):
        """Test PriorityCodeExist"""
        # FIXME: construct object with mandatory attributes with example values
        # model = priority_client.models.priority_code_exist.PriorityCodeExist()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
