# coding: utf-8

"""

    Backend utilities package

"""


from pathlib import Path

from setuptools import setup

NAME = "backend-utils"
VERSION = "1.0.0"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools
REQUIRES = [
    "PyYAML~=6.0",
    "openpyxl~=3.0.10",
    "python-magic~=0.4.26",
    "toolz~=0.11.2",
    "xlrd~=2.0.1",
    "country_client @ file://localhost/"
    f"{Path(__file__).parent / 'country_client'}",
    "priority_client @ file://localhost/"
    f"{Path(__file__).parent / 'priority_client'}",
]


setup(
    name=NAME,
    version=VERSION,
    description="",
    author_email="",
    url="",
    keywords=["utils", ""],
    package_data={"backend_utils": ["default_config.yaml"]},
    install_requires=REQUIRES,
    packages=["backend_utils"],
    include_package_data=True,
    long_description="""\
    backend utilities
    """,
)
