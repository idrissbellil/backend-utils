"""Provide utilities to the MVP backend"""
import csv
import functools
import io
from datetime import datetime
from importlib.resources import files
from math import isclose
from pathlib import Path
from typing import (Any, BinaryIO, Callable, Iterable, List, Mapping, Optional,
                    Tuple)

import magic
import xlrd
from openpyxl import load_workbook
from toolz import dicttoolz
from yaml import load

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

import country_client
import priority_client


def _parse_date(in_date: str):
    return datetime.strptime(in_date, "%m/%d/%Y").date()


_FIELD_TO_CONVERTER = {
    "region": str.lower,
    "country": str.lower,
    "item type": str.lower,
    "sales channel": str.lower,
    "order priority": str,
    "order date": _parse_date,
    "order id": int,
    "ship date": _parse_date,
    "units sold": int,
    "unit price": float,
    "unit cost": float,
    "total revenue": float,
    "total cost": float,
    "total profit": float,
}


def _normalize_entry_field(items: Tuple[str, Any]) -> Tuple[str, Any]:
    key, value = items
    new_key = key.lower()
    return (new_key, _FIELD_TO_CONVERTER[new_key](value))


def _normalize_entry(entry: Mapping[str, Any]):
    return dicttoolz.itemmap(_normalize_entry_field, entry)


def _read_csv(file_obj: BinaryIO) -> Iterable[Mapping[str, Any]]:
    csv_reader = csv.DictReader(io.TextIOWrapper(file_obj))
    for entry in csv_reader:
        yield _normalize_entry(entry)


def _read_xlsx(file_obj: BinaryIO) -> Iterable[Mapping[str, Any]]:
    workbook = load_workbook(filename=file_obj)
    worksheet = next(iter(workbook.worksheets))
    worksheeet_values_iter = iter(worksheet.values)
    header = next(worksheeet_values_iter)
    for row in worksheeet_values_iter:
        if None in row:
            # Some times there's a problem detecting the end of the
            # rows in excel 2007+
            break
        yield _normalize_entry(dict(zip(header, row)))


def _read_xls(file_obj: BinaryIO) -> Iterable[Mapping[str, Any]]:
    workbook = xlrd.open_workbook(file_contents=file_obj.read())
    worksheet = next(iter(workbook.sheets()))
    worksheeet_values_iter = iter(worksheet.get_rows())
    header = [cell.value for cell in next(worksheeet_values_iter)]
    for row in worksheeet_values_iter:
        row_values = [cell.value for cell in row]
        yield _normalize_entry(dict(zip(header, row_values)))


FILE_FORMAT_TO_READER: Mapping[Optional[str], Callable] = {
    "csv": _read_csv,
    "xls": _read_xls,
    "xlsx": _read_xlsx,
}


def _guess_file_format(file_obj: BinaryIO) -> Optional[str]:
    file_obj.seek(0)
    fmt = magic.from_buffer(file_obj.read(2048))
    file_obj.seek(0)
    if fmt == "CSV text":
        return "csv"
    # rely on xlrd for the rest. It returns <format> or `None`
    return xlrd.inspect_format(content=file_obj.read())


class FileFormatNotSuppoerted(Exception):
    """File format is not supported"""


def read_file(file_data: BinaryIO) -> Iterable[Mapping[str, Any]]:
    """Read the content of the file and"""
    file_format = _guess_file_format(file_data)
    file_data.seek(0)
    if file_format not in FILE_FORMAT_TO_READER:
        raise FileFormatNotSuppoerted(f"{file_format} not supported (yet?)")
    yield from FILE_FORMAT_TO_READER[file_format](file_data)


@functools.lru_cache()
def _cached_country_fetch(country, country_api_client):
    return country_api_client.root_get(country_name=country, async_req=True)


class CountryResultWrapper:
    """Wrap the results of the async country call"""

    def __init__(self, result, country_region):
        self._result = result
        self._country_region = country_region

    def get(self):
        country = next(iter(self._result.get()))
        return str.lower(country.region) == str.lower(self._country_region)


def _region_name_check(entry, country_api_client, **_):
    result = _cached_country_fetch(entry.get("country"), country_api_client)
    return CountryResultWrapper(result, str.lower(entry.get("region", "")))


@functools.lru_cache()
def _cached_priority_check(order_priority, priority_api_client):
    # Adding this shallow function to cache on order
    # priority rather than the entire entry
    return priority_api_client.priority_code_get(order_priority, async_req=True)


def _priority_code_check(entry, priority_api_client, **_):
    return _cached_priority_check(
        entry.get("order priority"), priority_api_client
    )


class FakeResult:
    """Provide a Future-like return value"""

    def __init__(self, result):
        self._result = result

    def get(self):
        return self._result


def _total_profit(entry, min_profit, **_):
    return FakeResult(
        isclose(entry.get("total profit", 0), min_profit)
        or entry.get("total profit", 0) > min_profit
    )


def _total_cost(entry, max_cost, **_):
    return FakeResult(
        isclose(entry.get("total cost", max_cost + 1), max_cost)
        or entry.get("total cost", max_cost + 1) < max_cost
    )


def _compare_order_ship_dates(entry, **_):
    return FakeResult(entry.get("order date") < entry.get("ship date"))


def _revenue_check(entry, **_):
    return FakeResult(
        isclose(
            entry.get("total revenue", 1),
            entry.get("units sold", 0) * entry.get("unit price", 0),
        )
    )


def _cost_check(entry, **_):
    return FakeResult(
        isclose(
            entry.get("total cost", 1),
            entry.get("units sold", 0) * entry.get("unit cost", 0),
        )
    )


_CHECK_TO_ERROR_MSG = [
    (_region_name_check, "region doesn't correspond to country"),
    (_priority_code_check, "priority does not exist"),
    (_total_profit, "total profit too low"),
    (_total_cost, "total cost too high"),
    (_revenue_check, "revenue is not equal to units sold x unit price"),
    (_compare_order_ship_dates, "order date is not less than ship date"),
    (_cost_check, "cost is not equal to units sold x unit cost"),
]


def _load_config(path: str) -> Mapping[str, Any]:
    with open(path) as config_f:
        return load(config_f, Loader=Loader)


def validate_dataset(
    dataset: Iterable[Mapping[str, Any]],
    config_path: Optional[str] = None,
) -> List[str]:
    """Validate the dataset"""
    _config_path = (
        config_path
        if config_path
        else str(files("backend_utils").joinpath("default_config.yaml"))
    )
    config = _load_config(_config_path)

    country_config = country_client.configuration.Configuration()
    country_config.host = config["country_api"]

    priority_config = priority_client.configuration.Configuration()
    priority_config.host = config["priority_api"]

    extra_args = {
        "country_api_client": country_client.DefaultApi(
            country_client.ApiClient(country_config)
        ),
        "priority_api_client": priority_client.DefaultApi(
            priority_client.ApiClient(priority_config)
        ),
        "min_profit": config["min_profit"],
        "max_cost": config["max_cost"],
    }
    results = []
    for i, entry in enumerate(dataset):
        for check, msg in _CHECK_TO_ERROR_MSG:
            results.append((i, msg, check(entry, **extra_args)))

    out_messages: List[str] = []
    for i, msg, result in results:
        try:
            is_valid = result.get()
        except country_client.rest.ApiException:
            out_messages.append(
                f"entry {i} -- failed to use country verification api"
            )
        except priority_client.rest.ApiException:
            out_messages.append(
                f"entry {i} -- failed to use priority verification api"
            )
        else:
            if not is_valid:
                out_messages.append(f"entry {i} -- {msg}")
    return out_messages
