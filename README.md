# backend-utils

Provide utilities for Backend

## Getting started

No Python versions other than `3.10` were tested.

### Usage

```bash
# 1. clone the repository
git clone git@gitlab.com:idrissbellil/backend-utils.git && cd backend-utils

# 2. in a new virtualenv
python -m pip install .
```

```python
from backend_utils import read_file, validate_dataset

file_obj = open('tests/test.xls', 'rb')
iter_data = read_file(file_obj)
validate_dataset(iter_data)
```

### Development

```bash
# in a new virtualenv
python -m pip install -U pip
python -m pip install -r requirements-dev.txt
pytest tests --cov=backend_utils
```

## TODO

* [X] Add / Configure VCR
* [X] Add / Configure Gitlab CI
* [X] Add `setup.py`
* [X] Move to Async/Futures for API calls
* [X] Switch relative data files access to `importlib_resources` (at least in `backend_utils/utils.py`)
* [ ] Improve type annotations
* [ ] Extract `country_api` to a new repo/package
* [ ] Extract `priority_api` to a new repo/package
* [ ] Attempt to increase the coverage from 94%
* [ ] Create / Upload pip package


## Notes

Although not requested, I added an extra keyword argument to
`validate_dataset` that can receive a configuration path, as
I didn't see a way to let some values be easily changeable
like the min revenue and the max cost.

Although the APIs were not provided yet, I felt the need to sketch
something on swaggerhub free trial account, as it seemed useful for
a more realistic testing scene.

The API is present [here](https://virtserver.swaggerhub.com/idriss-endpoint/rev-ai)

`VCR.py` cassesttes are already provided & the tests can run fully offline on
realistic data.

I took this a step further and generated the Python SDK clients using swagger codegen
as it provided far better functionality than what I can come up with in this limited
time. As a result, the SDK clients are shipped in this repo as well with minor
tweaks (made sure the name is no longer `swagger_client` for both for example).
