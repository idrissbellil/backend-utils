"""Test backend-utils utils module"""
import json

import pytest

from backend_utils.utils import read_file, validate_dataset


def test_read_csv_file(csv_file, file_content):
    assert list(read_file(csv_file)) == file_content


def test_read_xlsx_file(xlsx_file, file_content):
    assert list(read_file(xlsx_file)) == file_content


def test_read_xls_file(xls_file, file_content):
    assert list(read_file(xls_file)) == file_content


@pytest.mark.default_cassette("api_vcr_valid.yaml")
@pytest.mark.vcr
def test_validate_valid_dataset(valid_dataset):
    mesgs = validate_dataset(valid_dataset)
    assert [] == mesgs


@pytest.mark.default_cassette("api_vcr_invalid.yaml")
@pytest.mark.vcr
def test_validate_invalid_dataset(invalid_dataset):
    mesgs = validate_dataset(invalid_dataset)
    expected = [
        "entry 0 -- region doesn't correspond to country",
        "entry 0 -- priority does not exist",
        "entry 0 -- order date is not less than ship date",
        "entry 1 -- region doesn't correspond to country",
        "entry 1 -- priority does not exist",
        "entry 2 -- region doesn't correspond to country",
        "entry 2 -- priority does not exist",
    ]
    assert expected == mesgs


@pytest.mark.default_cassette("api_vcr_invalid.yaml")
@pytest.mark.vcr
def test_read_file_and_validate_dataset(xlsx_file):
    mesgs = validate_dataset(read_file(xlsx_file))
    expected = [
        "entry 0 -- region doesn't correspond to country",
        "entry 0 -- priority does not exist",
        "entry 1 -- region doesn't correspond to country",
        "entry 1 -- priority does not exist",
        "entry 2 -- region doesn't correspond to country",
        "entry 2 -- priority does not exist",
    ]
    assert expected == mesgs
