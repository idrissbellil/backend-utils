"""Provide general testing configs / fixtures"""

import datetime
from pathlib import Path

import pytest

BASE_PATH = Path(__file__).parent


@pytest.fixture()
def csv_file():
    return open(BASE_PATH / "test.csv", "rb")


@pytest.fixture()
def xls_file():
    return open(BASE_PATH / "test.xls", "rb")


@pytest.fixture()
def xlsx_file():
    return open(BASE_PATH / "test.xlsx", "rb")


@pytest.fixture()
def valid_dataset():
    return [
        {
            "region": "EU",
            "country": "Netherlands",
            "item type": "office supplies",
            "sales channel": "online",
            "order priority": "L",
            "order date": datetime.date(year=2011, month=1, day=27),
            "order id": 292494523,
            "ship date": datetime.date(year=2011, month=2, day=12),
            "units sold": 4484,
            "unit price": 651.21,
            "unit cost": 524.96,
            "total revenue": 2920025.64,
            "total cost": 2353920.64,
            "total profit": 566105.0,
        },
    ]


@pytest.fixture()
def file_content():
    return [
        {
            "region": "sub-saharan africa",
            "country": "chad",
            "item type": "office supplies",
            "sales channel": "online",
            "order priority": "L",
            "order date": datetime.date(year=2011, month=1, day=27),
            "order id": 292494523,
            "ship date": datetime.date(year=2011, month=2, day=12),
            "units sold": 4484,
            "unit price": 651.21,
            "unit cost": 524.96,
            "total revenue": 2920025.64,
            "total cost": 2353920.64,
            "total profit": 566105.0,
        },
        {
            "region": "europe",
            "country": "latvia",
            "item type": "beverages",
            "sales channel": "online",
            "order priority": "C",
            "order date": datetime.date(year=2015, month=12, day=28),
            "order id": 361825549,
            "ship date": datetime.date(year=2016, month=1, day=23),
            "units sold": 1075,
            "unit price": 47.45,
            "unit cost": 31.79,
            "total revenue": 51008.75,
            "total cost": 34174.25,
            "total profit": 16834.5,
        },
        {
            "region": "middle east and north africa",
            "country": "pakistan",
            "item type": "vegetables",
            "sales channel": "offline",
            "order priority": "C",
            "order date": datetime.date(year=2011, month=1, day=13),
            "order id": 141515767,
            "ship date": datetime.date(year=2011, month=2, day=1),
            "units sold": 6515,
            "unit price": 154.06,
            "unit cost": 90.93,
            "total revenue": 1003700.9,
            "total cost": 592408.95,
            "total profit": 411291.95,
        },
    ]


@pytest.fixture()
def invalid_dataset():
    return [
        {
            "region": "sub-saharan africa",
            "country": "chad",
            "item type": "office supplies",
            "sales channel": "online",
            "order priority": "L",
            "order date": datetime.date(year=2011, month=1, day=27),
            "order id": "292494523",
            "ship date": datetime.date(year=2011, month=1, day=12),
            "units sold": 4484,
            "unit price": 651.21,
            "unit cost": 524.96,
            "total revenue": 2920025.64,
            "total cost": 2353920.64,
            "total profit": 566105.0,
        },
        {
            "region": "europe",
            "country": "latvia",
            "item type": "beverages",
            "sales channel": "online",
            "order priority": "C",
            "order date": datetime.date(year=2015, month=12, day=28),
            "order id": "361825549",
            "ship date": datetime.date(year=2016, month=1, day=23),
            "units sold": 1075,
            "unit price": 47.45,
            "unit cost": 31.79,
            "total revenue": 51008.75,
            "total cost": 34174.25,
            "total profit": 16834.5,
        },
        {
            "region": "middle east and north africa",
            "country": "pakistan",
            "item type": "vegetables",
            "sales channel": "offline",
            "order priority": "C",
            "order date": datetime.date(year=2011, month=1, day=13),
            "order id": "141515767",
            "ship date": datetime.date(year=2011, month=2, day=1),
            "units sold": 6515,
            "unit price": 154.06,
            "unit cost": 90.93,
            "total revenue": 1003700.9,
            "total cost": 592408.95,
            "total profit": 411291.95,
        },
    ]
