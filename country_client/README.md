# swagger-client
No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

This Python package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0
- Package version: 1.0.0
- Build package: io.swagger.codegen.languages.PythonClientCodegen

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on Github, you can install directly from Github

```sh
pip install git+https://github.com//.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com//.git`)

Then import the package:
```python
import country_client 
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import country_client
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import country_client
from country_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = country_client.DefaultApi(country_client.ApiClient(configuration))
country_name = 'country_name_example' # str | Filter by country name (optional)

try:
    api_response = api_instance.root_get(country_name=country_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->root_get: %s\n" % e)

```

## Documentation for API Endpoints

All URIs are relative to *https://virtserver.swaggerhub.com/idriss-endpoint/rev-ai/1.0*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**root_get**](docs/DefaultApi.md#root_get) | **GET** / | 


## Documentation For Models

 - [Country](docs/Country.md)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author



