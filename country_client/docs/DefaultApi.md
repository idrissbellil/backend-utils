# country_client.DefaultApi

All URIs are relative to *https://virtserver.swaggerhub.com/idriss-endpoint/rev-ai/1.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**root_get**](DefaultApi.md#root_get) | **GET** / | 


# **root_get**
> list[Country] root_get(country_name=country_name)



Returns a list of countries. Country properties: code, name, description, region

### Example
```python
from __future__ import print_function
import time
import country_client
from country_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = country_client.DefaultApi()
country_name = 'country_name_example' # str | Filter by country name (optional)

try:
    api_response = api_instance.root_get(country_name=country_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->root_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country_name** | **str**| Filter by country name | [optional] 

### Return type

[**list[Country]**](Country.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

